/*****************************************************
 * Animator steuert die Wiedergabe von Bildsequenzen *
 * Er löst bei jedem Trigger-Signal am trigger()-Slot*
 * Ein changePicture-Signal mit dem passenden Pfad   *
 * nächsten Bilddatei aus.                           *
 *                                                   *
 * 2012, Andreas Rettig                              *
 ****************************************************/

#ifndef ANIMATOR_H
#define ANIMATOR_H
#include <QObject>

class Animator : public QObject
{
    Q_OBJECT

public:
    Animator(QString path, int numPictures, bool loop=false);

public slots:
    /* Trigger a new impulse for the animator */
    void trigger();
    /* Reset the animation to start from the beginning */
    void reset();
    /* Start the animation */
    void start();
    /* Stop the animation */
    void stop();
    /*  Show the next picture of the sequence */
    void showNextPicture();

signals:
    /* Emitted if it is time to load a new picture */
    void changePicture(QString path);

private:
    int ix;
    int numPictures;
    bool loop;
    bool running;
    QString filePath;

};

#endif // ANIMATOR_H
