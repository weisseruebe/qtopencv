#include "clickableqlabel.h"
#include <QMouseEvent>

ClickableQLabel::ClickableQLabel(QWidget *parent) :
    QLabel(parent)
{
    setMouseTracking(true);
}

void ClickableQLabel::mousePressEvent(QMouseEvent* event){
    emit clicked(event);
}

void ClickableQLabel::mouseReleaseEvent(QMouseEvent* event){
    emit released(event);
}

void ClickableQLabel::mouseMoveEvent(QMouseEvent* event){
    emit moved(event);
}
