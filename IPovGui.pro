#-------------------------------------------------
#
# Project created by QtCreator 2011-12-20T12:34:49
#
#-------------------------------------------------

QT       += core gui
QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.6.sdk

include(QextSerialPort.pri)

TARGET = IPovController
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    clickableqlabel.cpp \
    facetracker.cpp \
    positionaverager.cpp \
    mapper.cpp \
    facedetector.cpp \
    imageconverter.cpp \
    iPovDevice.cpp \
    animator.cpp

HEADERS  += mainwindow.h \
    clickableqlabel.h \
    facetracker.h \
    positionaverager.h \
    mapper.h \
    facedetector.h \
    imageconverter.h \
    iPovDevice.h \
    animator.h

FORMS    += mainwindow.ui

macx: LIBS += -L$$PWD/../../../../../opt/local/lib/ -lopencv_highgui
macx: LIBS += -L$$PWD/../../../../../opt/local/lib/ -lopencv_objdetect
macx: LIBS += -L$$PWD/../../../../../opt/local/lib/ -lopencv_imgproc
macx: LIBS += -L$$PWD/../../../../../opt/local/lib/ -lopencv_core
macx: LIBS += -L$$PWD/../../../../../opt/local/lib/ -lopencv_video
maxc: LIBS += -framework IOKit -framework CoreFoundation

INCLUDEPATH += $$PWD/../../../../../opt/local/include
DEPENDPATH += $$PWD/../../../../../opt/local/include

OTHER_FILES += \
    QextSerialPort.pri
