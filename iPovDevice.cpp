#include "iPovDevice.h"
#include "imageconverter.h"
#include <QtDebug>
#include <QMutex>
#include <QtConcurrentRun>

IPovDevice::IPovDevice(const QString & portName)
{
    this->port = new QextSerialPort(portName, QextSerialPort::EventDriven);
    port->setBaudRate(BAUD115200);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);
    if (port->open(QIODevice::ReadWrite) == true) {
        connect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    }
    else {
        qDebug() << "device failed to open:" << port->errorString();
    }
}

void IPovDevice::onReadyRead()
{
    QByteArray bytes;
    int a = port->bytesAvailable();
    bytes.resize(a);
    port->read(bytes.data(), bytes.size());

    if (bytes.size()==1 & bytes.at(0) == 'l'){
        emit(oneTurn());
    }
    emit(dataReceived(bytes));
}

int IPovDevice::write(char* val, int len)
{
    return port->write(val, len);
}

int IPovDevice::write(QByteArray data)
{
   return port->write(data);
}

int IPovDevice::write(char val)
{
    char c[1];
    c[0] = val;
    return port->write(c,1);
}


void IPovDevice::setOffset(int offset){
    this->offset = offset;
}

void IPovDevice::sendOffset(int offset){
    setOffset(offset);
    sendOffset();
}

void IPovDevice::sendOffset(){
    char* c = new char[2];
    c[0] = 'o';
    c[1] = offset;
    int res = port->write(c,2);
    //return res;
}

void IPovDevice::sendImage(QImage image){

    QByteArray imageData;
    imageData.append('d');
    imageData.append( ImageConverter::qImage2QByteArray(image));
    int res = write(imageData);
    // return res;
}

void IPovDevice::sendImageP(QString path){
        QImage qImage;
        if (qImage.load(path)){
            QFuture<void> f = QtConcurrent::run(this,&IPovDevice::sendImage,qImage);
        }
}
