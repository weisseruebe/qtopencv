/******************************************************
 * PositionAverager glättet einen Eingangswert durch  *
 * ein arithmetisches Mittel                          *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef POSITIONAVERAGER_H
#define POSITIONAVERAGER_H

#include <QObject>
#include "opencv2/core/core.hpp"

using namespace cv;

class PositionAverager: public QObject
{
    Q_OBJECT

public:
    PositionAverager();


public slots:
    /* Feed a new Face position */
    void onFaceTracked(RotatedRect rect);
    /* Set the smoothing strength */
    void setSmoothing(int iterations);

signals:
    /* Emitted for a new average position */
    void averagePosition(int x);

private:
    int i;
    int width;
    int* data;
};

#endif // POSITIONAVERAGER_H
