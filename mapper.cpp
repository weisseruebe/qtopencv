#include "mapper.h"

Mapper::Mapper()
{
    inMin = 0;
    outMin = 0;
    inMax = 100;
    outMax = 100;
}

void Mapper::inputValue(int d){
    float inDiff = inMax - inMin;
    int outDiff = outMax - outMin;
    int v = outMin + ((d/inDiff) * outDiff);
    emit mappedValue(v);
}

void Mapper::setOutMax(int v){
    outMax = v;
}

void Mapper::setOutMin(int v){
    outMin = v;
}

void Mapper::setInMax(int v){
    inMax = v;
}

void Mapper::setInMin(int v){
    inMin = v;
}

void Mapper::sendValues(){
    emit outMinChanged(outMin);
    emit outMaxChanged(outMax);
}
