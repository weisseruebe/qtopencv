#include "facedetector.h"


FaceDetector::FaceDetector()
{
    face_cascade_name = "/opt/local/share/opencv/haarcascades/haarcascade_frontalface_alt.xml";
    eyes_cascade_name = "/opt/local/share/opencv/haarcascades/haarcascade_eye_tree_eyeglasses.xml";

    if( !face_cascade.load( face_cascade_name ) ){
        qDebug("Error loading facecascade\n");
    }
    if( !eyes_cascade.load( eyes_cascade_name ) ){
        qDebug("Error loading eyes cascade\n");
    }
}

std::vector<Rect> FaceDetector::detectEyes(Mat image){
    std::vector<Rect> eyes;
    // In each face, detect eyes
    eyes_cascade.detectMultiScale( image, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30));
    if (eyes.size()==1 & !blinkTriggered){
        blinkTriggered = true;
        emit blinkDetected();
    } else if (eyes.size()==2){
        blinkTriggered = false;
    }
    return eyes;
}

void FaceDetector::paintObjects(Mat image, std::vector<Rect> objects, Rect middle){
    for( int j = 0; j < objects.size(); j++ )
    {
        Point center( middle.x + objects[j].x + objects[j].width*0.5, middle.y + objects[j].y + objects[j].height*0.5 );
        int radius = cvRound( (objects[j].width + objects[j].height)*0.25 );
        circle( image, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
    }
}

void FaceDetector::detectFaceWithEyes(Mat frame)
{
    std::vector<Rect> faces;
    Mat frame_gray;

    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    //imshow("B",frame_gray);
    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    for( int i = 0; i < faces.size(); i++ )
    {
        Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
        ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
        std::vector<Rect> eyes = detectEyes(frame_gray(faces[i]));
        paintObjects(frame,eyes,faces[i]);
        if (eyes.size()==2){
            emit faceWithEyesDetected(faces[i]);
        }
    }
}
