/******************************************************
 * Erweiterung der QLabel-Klasse, die Mausinteraktion *
 * erlaubt.                                           *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef CLICKABLEQLABEL_H
#define CLICKABLEQLABEL_H

#include <QLabel>

class ClickableQLabel : public QLabel
{
    Q_OBJECT

public:
   explicit ClickableQLabel(QWidget *parent = 0);
    
signals:
   void clicked(QMouseEvent* mouseEvent);
   void moved(QMouseEvent* moveEvent);
   void released(QMouseEvent* mouseEvent);

protected:
   void mousePressEvent(QMouseEvent* event);
   void mouseReleaseEvent(QMouseEvent* event);
   void mouseMoveEvent(QMouseEvent* event);
};

#endif // CLICKABLEQLABEL_H
