#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtConcurrentRun>
#include <QFuture>
#include <QTime>
#include <QTimer>

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("IPov Controller");

    detectFace = true;
    adjustMode = false;
    detectEyes = false;
    down = false;
    randomBlink = false;
    positionAverager = new PositionAverager();
    mapper = new Mapper();
    faceDetector = new FaceDetector();
    imageConverter = new ImageConverter();
    faceTracker = new FaceTracker();
    animator = new Animator("/Users/andreasrettig/workspace/iPov/images/auge/auge", 5);
    camera = cvCaptureFromCAM(0);

    fontFace = FONT_HERSHEY_PLAIN;
    RED = Scalar(0,0,255);
    GREEN = Scalar(0,255,0);
    YELLOW = Scalar(0,255,255);
    avgPos = 10;

    max = 80;
    min = 20;

    startTimer(40);
    qsrand(QTime::currentTime().msec());

    mapper->setInMax(640);
    mapper->setOutMax(100);
    mapper->setOutMin(0);

    connect(ui->lblImage,SIGNAL(moved(QMouseEvent*)),this,SLOT(on_mouseMoved(QMouseEvent*)));
    connect(ui->lblImage,SIGNAL(clicked(QMouseEvent*)),this,SLOT(on_mouseDown(QMouseEvent*)));
    connect(ui->lblImage,SIGNAL(released(QMouseEvent*)),this,SLOT(on_mouseUp(QMouseEvent*)));
    connect(ui->sliderOutMin,SIGNAL(valueChanged(int)),mapper,SLOT(setOutMin(int)));
    connect(ui->sliderOutMax,SIGNAL(valueChanged(int)),mapper,SLOT(setOutMax(int)));
    connect(ui->sliderSMin,SIGNAL(valueChanged(int)),faceTracker,SLOT(setSMin(int)));
    connect(ui->sliderVMin,SIGNAL(valueChanged(int)),faceTracker,SLOT(setVMin(int)));
    connect(ui->sliderVMax,SIGNAL(valueChanged(int)),faceTracker,SLOT(setVMax(int)));
    connect(ui->sliderSmooth,SIGNAL(valueChanged(int)),positionAverager,SLOT(setSmoothing(int)));

    connect(ui->sliderSMin,SIGNAL(sliderPressed()),this,SLOT(setAdjustModeOn()));
    connect(ui->sliderVMin,SIGNAL(sliderPressed()),this,SLOT(setAdjustModeOn()));
    connect(ui->sliderVMax,SIGNAL(sliderPressed()),this,SLOT(setAdjustModeOn()));
    connect(ui->sliderSMin,SIGNAL(sliderReleased()),this,SLOT(setAdjustModeOff()));
    connect(ui->sliderVMin,SIGNAL(sliderReleased()),this,SLOT(setAdjustModeOff()));
    connect(ui->sliderVMax,SIGNAL(sliderReleased()),this,SLOT(setAdjustModeOff()));

    connect(faceTracker,SIGNAL(vmaxChanged(int)),ui->sliderVMax,SLOT(setValue(int)));
    connect(faceTracker,SIGNAL(vminChanged(int)),ui->sliderVMin,SLOT(setValue(int)));
    connect(faceTracker,SIGNAL(sminChanged(int)),ui->sliderSMin,SLOT(setValue(int)));

    connect(mapper,SIGNAL(outMaxChanged(int)),ui->sliderOutMax,SLOT(setValue(int)));
    connect(mapper,SIGNAL(outMinChanged(int)),ui->sliderOutMin,SLOT(setValue(int)));

    connect(ui->pushButton,SIGNAL(clicked()),animator,SLOT(reset()));
    connect(ui->checkBox,SIGNAL(stateChanged(int)),this,SLOT(setBlinkDetection(int)));
    connect(ui->checkBox_2,SIGNAL(stateChanged(int)),this,SLOT(setRandomBlink(int)));

    connect(ui->pushButtonReset,SIGNAL(clicked()),this,SLOT(resetFaceTracking()));
    connect(ui->pushButtonResetFacetracking,SIGNAL(clicked()),faceTracker,SLOT(setDefaults()));

    connect(faceDetector,SIGNAL(faceWithEyesDetected(Rect)),this,SLOT(startFaceTracking(Rect)));
    connect(faceDetector,SIGNAL(blinkDetected()),animator,SLOT(reset()));

    connect(faceTracker,SIGNAL(faceTracked(RotatedRect)),this,SLOT(showFacePosition(RotatedRect)));
    connect(faceTracker,SIGNAL(faceTracked(RotatedRect)),positionAverager,SLOT(onFaceTracked(RotatedRect)));
    connect(faceTracker,SIGNAL(faceLost()),this,SLOT(resetFaceTracking()));

    connect(positionAverager,SIGNAL(averagePosition(int)),mapper,SLOT(inputValue(int)));
    connect(mapper,SIGNAL(mappedValue(int)),this,SLOT(showFaceAveragePosition(int)));

    faceTracker->sendValues();
    mapper->sendValues();
    this->triggerRandomBlink();
}

void MainWindow::setIPovDevice(IPovDevice* p){
    iPov = p;
    connect(mapper,SIGNAL(mappedValue(int)),iPov,SLOT(sendOffset(int)));
    connect(iPov,SIGNAL(oneTurn()),animator,SLOT(trigger()));
    connect(animator,SIGNAL(changePicture(QString)),iPov,SLOT(sendImageP(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent*){
    IplImage *image=cvQueryFrame(camera);
    Mat src(image);
    Mat picToPaint(image);
    if (down){
        rectangle(src,Rect(startX,startY,x-startX,y-startY),RED);
    }
    //Passiert immer, damit der Tracker sofort ein Histogramm hat.
    faceTracker->trackFace(src);
    if (detectFace){
        faceDetector->detectFaceWithEyes(src);
        putText(picToPaint,"Detecting",Point(1,15),fontFace,1,RED);

    } else {
        picToPaint = adjustMode ? faceTracker->backproj : src;
        if (faceRect.size.width>0 && faceRect.size.height>0){
            ellipse(picToPaint, faceRect, RED, 3, CV_AA );
        }
        line(picToPaint,Point(avgPos,picToPaint.size().height),Point(avgPos,picToPaint.size().height-20),YELLOW);
        putText(picToPaint,"Tracking",Point(1,15),fontFace,1,GREEN);

        if (detectEyes){
            detectAndPaintEyes(faceRect.boundingRect(),src,picToPaint);
        }
    }

    IplImage ipl_img = picToPaint;
    setImage(&ipl_img);
}

void MainWindow::detectAndPaintEyes(Rect rect, Mat src, Mat picToPaint){
    /*Eye detection in face rect*/
    if (rect.x>0 && rect.y>0 && rect.x+rect.width <= src.cols && rect.y+rect.height <= src.rows){
        std::vector<Rect> eyes = faceDetector->detectEyes(src(rect));
        if (eyes.size()>0){
            faceDetector->paintObjects(picToPaint,eyes,rect);
        }
    }
}

void MainWindow::setImage(IplImage* image){
    ui->lblImage->setPixmap(QPixmap::fromImage(ImageConverter::iplImage2QImage(image)));
}


void MainWindow::on_mouseMoved(QMouseEvent* event){
    this->x = event->pos().x()-ui->lblImage->geometry().x();
    this->y = event->pos().y()-ui->lblImage->geometry().y();
}

void MainWindow::on_mouseDown(QMouseEvent* event){
    down = true;
    startX = event->pos().x();
    startY = event->pos().y();
}

void MainWindow::on_mouseUp(QMouseEvent* event){
    down = false;
    detectFace = false;
    faceTracker->setFaceRegion(Rect(startX,startY,event->pos().x()-startX,event->pos().y()-startY));
}


void MainWindow::showFacePosition(RotatedRect rect){
    faceRect = rect;
}

void MainWindow::startFaceTracking(Rect face)
{
    detectFace = false;
    faceTracker->setFaceRegion(face);
}

void MainWindow::showFaceAveragePosition(int x)
{
    avgPos = x;
}

void MainWindow::resetFaceTracking()
{
    detectFace = true;
}

void MainWindow::setAdjustModeOn(){
    adjustMode = true;
}

void MainWindow::setAdjustModeOff(){
    adjustMode = false;
}

void MainWindow::setBlinkDetection(int state)
{
    detectEyes = state;
}

void MainWindow::setRandomBlink(int state)
{
    randomBlink = state;
    triggerRandomBlink();
}

void MainWindow::triggerRandomBlink(){
    if (randomBlink){
        animator->reset();
        int r = qrand()/0xffff;
        int t = r*(max-min+1.0)/(RAND_MAX/0xffff+1.0)+min;
        QTimer::singleShot(t*100,this,SLOT(triggerRandomBlink()));
    }
}
