/******************************************************
 * Mainwindows ist initialisiert die GUI und die      *
 * Pipeline und fragt Bilder von der Kamera ab, um    *
 * sie zur Verarbeitung zu leiten.                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <QMouseEvent>
#include <stdlib.h>
#include <stdio.h>

#include "facetracker.h"
#include "positionaverager.h"
#include "mapper.h"
#include "facedetector.h"
#include "imageconverter.h"
#include "animator.h"
#include "iPovDevice.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setImage(IplImage* image);
    ~MainWindow();
    void timerEvent(QTimerEvent *);
    void setIPovDevice(IPovDevice* p);
    void detectAndPaintEyes(Rect rect, Mat src, Mat picToPaint);

public slots:
    void showFacePosition(RotatedRect rect);
    void showFaceAveragePosition(int x);

private slots:
    void on_mouseMoved(QMouseEvent* event);
    void on_mouseUp(QMouseEvent* event);
    void on_mouseDown(QMouseEvent* event);
    void startFaceTracking(Rect face);
    void setAdjustModeOn();
    void setAdjustModeOff();
    void resetFaceTracking();
    void setBlinkDetection(int state);
    void setRandomBlink(int state);
    void triggerRandomBlink();

private:
    Ui::MainWindow *ui;
    CvCapture * camera;
    IPovDevice* iPov;
    int x,y,startX,startY;
    float max, min;
    int avgPos;
    bool down;
    bool detectFace;
    bool adjustMode;
    bool detectEyes;
    bool randomBlink;

    int fontFace;
    Scalar RED;
    Scalar GREEN;
    Scalar YELLOW;

    RotatedRect faceRect;
    PositionAverager* positionAverager;
    Mapper* mapper;
    FaceDetector* faceDetector;
    ImageConverter* imageConverter;
    FaceTracker* faceTracker;
    Animator* animator;
};

#endif // MAINWINDOW_H
