#include <QtGui/QApplication>
#include "mainwindow.h"
#include "opencv/highgui.h"
#include "opencv/cv.h"
#include "iPovDevice.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString portName = "/dev/tty.WiiCopter-DevB";
    IPovDevice* listener = new IPovDevice(portName);

    MainWindow w;
    w.setIPovDevice(listener);
    w.show();
    return a.exec();
}
