#include "facetracker.h"

float hranges[] = {0,180};
const float* phranges = hranges;
int histoSize = 16;
int minFaceSize = 5;
Rect trackWindow;
bool trackObject = false;

FaceTracker::FaceTracker()
{
    setDefaults();
    hsv, hue, mask, hist, backproj = Mat::zeros(200, 320, CV_8UC3);
}

void FaceTracker::trackFace(Mat image){
    calculateMask(image);
    if (trackObject){
        if (trackWindow.height>0 && trackWindow.width>0)
        {
            calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
            backproj &= mask;
            RotatedRect trackBox = CamShift(backproj, trackWindow, TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));
            if (trackBox.size.height < minFaceSize){
                trackObject = false;
                emit faceLost();
            }
            emit faceTracked(trackBox);
        } else {
            trackObject = false;
            emit faceLost();
        }
    }
}

void FaceTracker::setFaceRegion(Rect selection){
    Mat roi(hue, selection), maskroi(mask, selection);
    calcHist(&roi, 1, 0, maskroi, hist, 1, &histoSize, &phranges);
    normalize(hist, hist, 0, 255, CV_MINMAX);
    trackWindow = selection;
    trackObject = true;
}

void FaceTracker::calculateMask(Mat image){
    cvtColor(image, hsv, CV_BGR2HSV);

    /* Pixel im Bereich maskieren */
    inRange(hsv, Scalar(0, smin, MIN(vmin,vmax)),Scalar(180, 256, MAX(vmin, vmax)), mask);
    int ch[] = {0, 0};
    hue.create(hsv.size(), hsv.depth());
    mixChannels(&hsv, 1, &hue, 1, ch, 1);
}

void FaceTracker::setVMax(int vmax){
    this->vmax = vmax;
    emit vmaxChanged(vmax);

}

void FaceTracker::setVMin(int vmin){
    this->vmin = vmin;
    emit vminChanged(vmin);
}

void FaceTracker::setSMin(int smin){
    this->smin = smin;
    emit sminChanged(smin);
}

void FaceTracker::sendValues(){
    emit vminChanged(vmin);
    emit vmaxChanged(vmax);
    emit sminChanged(smin);
}

void FaceTracker::setDefaults(){
    vmin = 10;
    vmax = 255;
    smin = 30;
    sendValues();
}
