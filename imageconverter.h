/******************************************************
 * ImageConverter bietet Methoden zur Umwandlung      *
 * verschiedener Bildformate                          *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef IMAGECONVERTER_H
#define IMAGECONVERTER_H

#include <QObject>
#include <QImage>
#include <opencv/cv.h>
#include <opencv/highgui.h>

class ImageConverter : public QObject
{
    Q_OBJECT

public:
    /* Converts a QImage to a QByteArray for the iPov */
    static QByteArray qImage2QByteArray(QImage image);

    /* Converts an OpenCV iplImage to a QImage for display in QT */
    static QImage iplImage2QImage(IplImage *cvimage);

};

#endif // IMAGECONVERTER_H
