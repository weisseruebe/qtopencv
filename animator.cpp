#include "animator.h"

Animator::Animator(QString path, int numPictures, bool loop)
{
    ix = 0;
    this->numPictures = numPictures;
    this->loop = loop;
    filePath = path;
    running = true;
}

void Animator::trigger()
{
    if (running){
        if (loop){
            showNextPicture();
            ix = (ix+1)%numPictures;
        } else {
            if (ix<numPictures){
                showNextPicture();
                ix++;
            }
        }
    }
}

void Animator::showNextPicture(){
    QString newPath = filePath;
    newPath.append(QString::number(ix));
    newPath.append(".bmp");
    emit(changePicture(newPath));
}

void Animator::reset(){
    ix = 0;
}

void Animator::start(){
    running = true;
}

void Animator::stop(){
    running = false;
}
