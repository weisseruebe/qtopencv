/******************************************************
 * IPovDevice ist die Hardwareschnittstelle zur iPov- *
 * Hardware                                           *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef PORTLISTENER_H_
#define PORTLISTENER_H_

#include <QObject>
#include <QImage>
#include "qextserialport.h"

class IPovDevice : public QObject
{
    Q_OBJECT

public:
    IPovDevice(const QString & portName);
    int write(char *val, int len);
    int write(QByteArray data);
    int write(char val);
    void sendImage(QImage image);

private:
    QextSerialPort *port;
    int offset;

public slots:
    /* Send the current offset */
    void sendOffset();
    /* Set the current offset without sending */
    void setOffset(int offset);
    /* Set and send the current offset */
    void sendOffset(int offset);
    /* Send an image loaded from the file at path*/
    void sendImageP(QString path);

signals:
    /* Emitted once a turn */
    void oneTurn();
    void dataReceived(QByteArray bytes);

private slots:
    void onReadyRead();
};

#endif /*PORTLISTENER_H_*/
