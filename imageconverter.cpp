#include "imageconverter.h"
#include <QImage>

static int BLACK = -16777216;

QByteArray ImageConverter::qImage2QByteArray(QImage image){
    QByteArray buffer(2*image.width(),(char)0);
    int bufferPos = 0;
    for (int x=0;x<image.width();x++){
        for (int y=0;y<image.height();y+=8){
            for (int yB=0;yB<8;yB++){
                QRgb rgb = image.pixel(x,yB+y);
                if (rgb == BLACK){
                    quint8 mask = 128 >> yB;
                    buffer[bufferPos] = (char)(mask | buffer[bufferPos]);
                }
            }
            bufferPos++;
        }
    }
    return buffer;
}

/* Quelle http://www.developer.nokia.com/Community/Wiki/Using_OpenCV_with_Qt*
   Erweitert um 1-Kanal Quellbilder
*/
QImage ImageConverter::iplImage2QImage(IplImage *cvimage) {
    int cvIndex, cvLineStart;
    QImage image;
    // switch between bit depths
    switch (cvimage->depth) {
    case IPL_DEPTH_8U:
        switch (cvimage->nChannels) {
        case 3:
            if ( (cvimage->width != image.width()) || (cvimage->height != image.height()) ) {
                QImage temp(cvimage->width, cvimage->height, QImage::Format_RGB32);
                image = temp;
            }
            cvIndex = 0; cvLineStart = 0;
            for (int y = 0; y < cvimage->height; y++) {
                unsigned char red,green,blue;
                cvIndex = cvLineStart;
                for (int x = 0; x < cvimage->width; x++) {
                    red = cvimage->imageData[cvIndex+2];
                    green = cvimage->imageData[cvIndex+1];
                    blue = cvimage->imageData[cvIndex];

                    image.setPixel(x,y,qRgb(red, green, blue));
                    cvIndex += 3;
                }
                cvLineStart += cvimage->widthStep;
            }
            break;
        case 1:
            if ( (cvimage->width != image.width()) || (cvimage->height != image.height()) ) {
                QImage temp(cvimage->width, cvimage->height, QImage::Format_RGB32);
                image = temp;
            }
            cvIndex = 0; cvLineStart = 0;
            for (int y = 0; y < cvimage->height; y++) {
                unsigned char red,green,blue;
                cvIndex = cvLineStart;
                for (int x = 0; x < cvimage->width; x++) {
                    red = cvimage->imageData[cvIndex];
                    green = cvimage->imageData[cvIndex];
                    blue = cvimage->imageData[cvIndex];

                    image.setPixel(x,y,qRgb(red, green, blue));
                    cvIndex ++;
                }
                cvLineStart += cvimage->widthStep;
            }
            break;
        default:
            qDebug("This number of channels is not supported %d",cvimage->nChannels);
            break;
        }
        break;
    default:
        qDebug("This type of IplImage is not implemented in Imageconverter");
        break;
    }
    return image;
}

