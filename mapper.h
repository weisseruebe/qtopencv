/******************************************************
 * Mapper skaliert einen Eingangswert auf einen       *
 * anderen Wertebereich                               *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef MAPPER_H
#define MAPPER_H

#include <QObject>
using namespace std;

class Mapper : public QObject
{
     Q_OBJECT

public:
    Mapper();


public slots:
    /* Feed a value */
    void inputValue(int d);
    void setOutMax(int v);
    void setOutMin(int v);
    void setInMax(int v);
    void setInMin(int v);

    void sendValues();

signals:
    /* Emitted for a new mapped value */
    void mappedValue(int d);

    void outMaxChanged(int v);
    void outMinChanged(int v);

private:
    int outMin;
    int outMax;
    int inMin;
    int inMax;
};

#endif // MAPPER_H
