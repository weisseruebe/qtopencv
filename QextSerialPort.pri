INCLUPATH += SerialPort

SOURCES += \
    qextserialport.cpp \


unix:SOURCES           += posix_qextserialport.cpp
unix:!macx:SOURCES     += qextserialenumerator_unix.cpp

windows{
    SOURCES += win_qextserialport.cpp
    SOURCES += qextserialenumerator_win.cpp
}

macx {
  SOURCES          += qextserialenumerator_osx.cpp
  LIBS             += -framework IOKit -framework CoreFoundation
}

HEADERS += \
    qextserialport_global.h \
    qextserialport.h \
    qextserialenumerator.h

