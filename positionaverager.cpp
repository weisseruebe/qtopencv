#include "positionaverager.h"


PositionAverager::PositionAverager()
{
     this->width = 3;
     this->data = new int[255];
}

void PositionAverager::setSmoothing(int iterations){
    this->width = iterations;
}

void PositionAverager::onFaceTracked(RotatedRect rect){
    data[i++ % width] = rect.center.x;
    int sum = 0;
    for (int x=0;x<width;x++){
        sum += data[x];
    }
    emit averagePosition(sum/width);
}
