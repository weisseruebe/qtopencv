/******************************************************
 * FaceDetector sucht im übergebenden Bild nach einem *
 * Gesicht mit zwei Augen und löst beim ersten        *
 * erkannten Gesicht faceWithEyesDetected aus.        *
 * Ausserdem bietet es separate Augenerkennung.       *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include <QObject>
#include <iostream>
#include <stdio.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

class FaceDetector : public QObject
{
    Q_OBJECT

public:
    FaceDetector();
    void detectFaceWithEyes(Mat frame);
    std::vector<Rect> detectEyes(Mat image);
    void paintObjects(Mat image, std::vector<Rect> objects, Rect middle);

private:
    CascadeClassifier face_cascade;
    CascadeClassifier eyes_cascade;
    String face_cascade_name;
    String eyes_cascade_name;
    bool blinkTriggered;

signals:
    void faceWithEyesDetected(Rect face);
    void blinkDetected();
};

#endif // FACEDETECTOR_H
