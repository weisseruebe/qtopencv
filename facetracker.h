/******************************************************
 * FaceTracker trackt Objekte bzw. Gesichter anhand   *
 * Histogramm Backprojektion und CamShift-Algorithmus *
 *                                                    *
 * 2012, Andreas Rettig                               *
 *****************************************************/

#ifndef FACETRACKER_H
#define FACETRACKER_H

#include <ctype.h>
#include <QObject>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/core/core.hpp"

using namespace cv;
using namespace std;

class FaceTracker : public QObject
{
    Q_OBJECT

public:
    FaceTracker();
    void calculateMask(Mat image);
    void setFaceRegion(Rect selection);
    void trackFace(Mat image);
    Mat backproj;

public slots:
    /* Set the tracker parameters */
    void setVMax(int vmax);
    void setVMin(int vmin);
    void setSMin(int smin);

    /* Send the current values */
    void sendValues();

    /* Reset the parameters to defaults */
    void setDefaults();

signals:
    void faceTracked(RotatedRect rect);
    void faceLost();
    void vmaxChanged(int v);
    void vminChanged(int v);
    void sminChanged(int v);

private:
    int  vmin;
    int  vmax;
    int  smin;
    Mat hsv, hue, mask, hist;

};

#endif // FACETRACKER_H
